﻿#include <iostream>
#include <math.h>

class Primer
{
private:
    int a = 120;
public:
    int PrintA()
    {
        return a;
    }
};

class Vector
{
private:
    double x = 5;
    double y = 2;
    double z = 9;
public:
    double Module() 
    {
        double ans = sqrt(x*x + y*y + z*z);
        return ans;
    }
};

int main()
{
    Primer test1;
    std::cout << test1.PrintA();

    std::cout << std::endl;

    Vector test2;
    std::cout << test2.Module();
}


